let addCard = (json) => {
    let section = document.getElementsByTagName("section")[0];
    let divParent = createElement('div')

    addElementInto(section, divParent)
    addElementInto(divParent, createElement('img', [
        {src: json.picture ? json.picture : "./img/studients/default-user-img.jpg"},
        {alt: "User's picture"}]))
    addElementInto(divParent, createElement('h3', [ {innerHTML: json.prenom+" "+json.nom} ]))
    addElementInto(divParent, createElement('h4', [ {innerHTML: json.description ? json.description : "SOON"} ]))

    let ul = createElement('ul', [ {className: 'fl' }])
    addElementInto(divParent, ul)
    addSocialIcons(ul, [ 'discord', "mel", "git", "linkedin" ], json)
}

let createElement = (elementType, options = null ) => {
    let element = document.createElement(elementType);
    
    if(options != null)
        options.map(json => {
            let opt = Object.keys(json)[0];
            element[opt] = json[opt]
        })
    return element
}

let addElementInto = (parent, child) => {
    parent.appendChild(child);
}

let addSocialIcons = (ul, socials = [], json) => {
    socials.map(social => {
        if(json[social]){
            let li = createElement('li', [ {id: json[social]} ])
            addElementInto(ul, li)
            addElementInto(li, createElement('i', [ {className: socialToIcons[social]} ]))
            addClickEvent(li)
        }
    })
}

let addClickEvent = (e) =>{
    e.addEventListener('click', el => alert(e.id))
}
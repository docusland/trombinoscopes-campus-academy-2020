let socialToIcons = {
    "discord": "fab fa-discord",
    "mel": "fas fa-envelope-open",
    "git": "fab fa-github",
    "linkedin": "fab fa-linkedin"
}

let users = [
    {
        "nom": "Duclos",
        "prenom": "Erwann",
        "picture": "https://www.nautiljon.com/images/perso/00/65/tom_sawyer_18656.jpg",
        "description": "Le grand PadErwan",
        "mel": "erwann.duclos@campus.academy",
        "discord": "Erwann#8087",
        "git": "https://gitlab.com/docusland"
    },
    {
        "nom": "Dufeu",
        "prenom": "Corentin",
        "description": "Je suis trop chaud !",
        "mel": "corentin.dufeu@campus.academy",
        "discord": "Corentin.dufeu#3223"
    },
    {
        "nom": "Thomas",
        "prenom": "Valente",
        "mel": "thomas.valente@campus.academy",
        "discord": "Samøth#2788"
    },
    {
        "nom": "Corre",
        "prenom": "Telio",
        "mel": "telio.corre@campus.academy",
        "discord": "Télio#5045"
    },
    {
        "nom": "Collin",
        "prenom": "Tom",
        "mel": "tom.collin@campus.academy",
        "discord": "Toumou#5079"
    },
    {
        "nom": "Dore",
        "prenom": "Mathis",
        "mel": "mathis.dore@campus.academy",
        "discord": "Asario#6129"
    },
    {
        "nom": "Rousseau",
        "prenom": "Clair",
        "picture": "https://static.wikia.nocookie.net/lemondededisney/images/1/17/Cendrillon.png/revision/latest?cb=20120731205301&path-prefix=fr",
        "mel": "clair.rousseau@campus.academy",
        "discord": "Claire_R#2554"
    },
    {
        "nom": "Morel",
        "prenom": "Lilian",
        "mel": "lilian.morel@campus.academy",
        "discord": "GoldAcroZ#9835"
    },
    {
        "nom": "Roue",
        "prenom": "Auréli",
        "picture": "https://www.coup-de-vieux.fr/wp-content/uploads/2009/08/fa7ba80a.jpg",
        "mel": "aurelie.roue@campus.academy",
        "discord": "Aurélie#3785"
    },
    {
        "nom": "Tomine",
        "prenom": "Romain",
        "mel": "romain.tomine@campus.academy",
        "discord": "Wasomo#4123"
    },
    {
        "nom": "Falempin",
        "prenom": "Kendal",
        "mel": "kendal.falempin@campus.academy",
        "discord": "TheTetonFire#7968"
    },
    {
        "nom": "Perrier",
        "prenom": "Nathan",
        "mel": "nathan.perrier@campus.academy",
        "discord": "Rekyt#2099"
    },
    {
        "nom": "Pontais",
        "prenom": "Gwladys",
        "mel": "gwladys.pontais@campus.academy",
        "discord": "Hidjuka#7953"
    },
    {
        "nom": "Josselin",
        "prenom": "Gayot",
        "mel": "josselin.gayot@campus.academy",
        "discord": "Blackcrow#9569"
    },
    {
        "nom": "Legrand",
        "prenom": "Anthony",

        "picture": "https://static1-fr.millenium.gg/articles/8/36/18/48/@/1312725-portrait-doom-slayer-article_m-1.jpg",
        "description": "#Brayan",
        "mel": "anthony-legrand@live.fr",
        "discord": "BowEnder#7710"
    },
    {
        "nom": "Morel",
        "prenom": "Theo",
        "mel": "theo.morel@campus.academy",
        "discord": "theomomo#4706"
    },
    {
        "nom": "Hamon",
        "prenom": "Florian",
        "mel": "florian.hamon@campus.academy",
        "discord": "Sympion#4446"
    },
    {
        "nom": "Lamon",
        "prenom": "Thibaud",
        "mel": "thibaud.lamon@campus.academy",
        "discord": "Thibaud LAMON#2682"
    }
]
window.onload = () => {
    setTimeout(()=>{
        document.getElementById('BowEnder#7710').addEventListener('mouseover', ()=>{
            if(document.getElementById('egg')) return;
            let body = document.getElementsByTagName('body')[0]
            let audio = createElement('audio', [ {preload: 'auto'}, {id: 'egg'} ])
            addElementInto(body, audio)
            addElementInto(audio, createElement('source', [ {src: './img/videoplayback.mp4'} ]))

            document.getElementById("egg").play()
            let basicImg = "https://static1-fr.millenium.gg/articles/8/36/18/48/@/1312725-portrait-doom-slayer-article_m-1.jpg";
            let r = Array.from(document.getElementsByTagName('img')).filter(img => img.src == basicImg)
            r[0].src = "https://d2tcpjlev2skwu.cloudfront.net/wp-content/uploads/2016/11/31101812/adventure_easteregg-1024x686.jpg"

            setTimeout(()=>{
                r[0].src = basicImg
                audio.remove()
            }, 2800)
        })
    }, 1000)
}
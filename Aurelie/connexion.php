<!DOCTYPE html>
<html lang="fr">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/style2.css">
        <title>Trombinoscope</title>
    </head>

    <body>
        <div id="container">
            <form action="verification.php" method="POST">
                <h1>Connexion</h1>
                    <p/>
                    <label><b>Nom d'utilisateur</b></label>
                    <input type="text" placeholder="Entrer le nom d'utilisateur" name="username" required><br/>
                    <label><b>Mot de passe</b></label>
                    <input type="password" placeholder="Entrer le mot de passe" name="password" required><br/>
                    <p/>
                    <input type="submit" id='submit' value='Login' >
                        <?php
                            if(isset($_GET['erreur'])){
                                $err = $_GET['erreur'];
                                if($err==1 || $err==2) {
                                    echo "<p style='color:red'>Utilisateur ou mot de passe incorrect</p>";
                                }
                            }
                        ?>
                    <p/>
                    <form method='POST' action='fenetre.php'>
                        Je n'ai pas de compte :
                        <input type='checkbox' name='case' value='on'>
                    </form>
            </form>
            </div>
    </body>

</html>
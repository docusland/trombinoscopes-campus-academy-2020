<!doctype html>
<html lang="fr">

    <head>
        <script src="https://kit.fontawesome.com/dcdf2d08ba.js" crossorigin="anonymous"></script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/style.css">
        <title>Trombinoscope</title>
    </head>

    <body>
        <header>
            <?php include("menu.php"); ?>
            <a href="#">
                <img class="logo" src="./assets/images/logo.png" alt="Logo">
            </a>
            <h1>Trombinoscope</h1>
        </header>

        <div class="container">
            <aside>
                <h2>&nbspPromotion</h2>
                <p>&nbsp2020-2023</p>
                <p class="hide-sm">&nbspB1 Switch IT</p>
                <br>
                <h3 class="hide-sm">&nbspSlogan</h3>
                </br>
                <p class="hide-sm">&nbspEnsemble pour réussir.</p>
                <?php
                    include("connexion.php");
                ?>
            </aside>

            <div class="students">
                <a href="#" class="student">
                    <!--image,texte-->
                    <img src="./assets/images/User_Avatar_2.png" alt="">
                    <span>Anthony</span>
                    <div class="social-network">
                        <i class="fab fa-discord"></i>
                        <i class="fab fa-linkedin"></i>
                        <i class="fas fa-at"></i>
                    </div>
                    <textarea cols="15" rows="2">Présentation :</textarea>
                </a>
                <a href="#" class="student">
                    <!--image,texte-->
                    <img src="./assets/images/User_Avatar_2.png" alt="">
                    <span>Arthur</span>
                    <div class="social-network">
                        <i class="fab fa-discord"></i>
                        <i class="fab fa-linkedin"></i>
                        <i class="fas fa-at"></i>
                    </div>
                    <textarea cols="15" rows="2">Présentation :</textarea>
                </a>
                <a href="#" class="student">
                    <!--image,texte-->
                    <img src="./assets/images/User_Avatar_2.png" alt="">
                    <span>Aurélie</span>
                    <div class="social-network">
                        <i class="fab fa-discord"></i>
                        <i class="fab fa-linkedin"></i>
                        <i class="fas fa-at"></i>
                    </div>
                    <textarea cols="15" rows="2">Présentation :</textarea>
                </a>
                <a href="#" class="student">
                    <!--image,texte-->
                    <img src="./assets/images/User_Avatar_2.png" alt="">
                    <span>Claire</span>
                    <div class="social-network">
                        <i class="fab fa-discord"></i>
                        <i class="fab fa-linkedin"></i>
                        <i class="fas fa-at"></i>
                    </div>
                    <textarea cols="15" rows="2">Présentation :</textarea>
                </a>
                <a href="#" class="student">
                    <!--image,texte-->
                    <img src="./assets/images/User_Avatar_2.png" alt="">
                    <span>Corentin</span>
                    <div class="social-network">
                        <i class="fab fa-discord"></i>
                        <i class="fab fa-linkedin"></i>
                        <i class="fas fa-at"></i>
                    </div>
                    <textarea cols="15" rows="2">Présentation :</textarea>
                </a>
                <a href="#" class="student">
                    <!--image,texte-->
                    <img src="./assets/images/User_Avatar_2.png" alt="">
                    <span>Florian</span>
                    <div class="social-network">
                        <i class="fab fa-discord"></i>
                        <i class="fab fa-linkedin"></i>
                        <i class="fas fa-at"></i>
                    </div>
                    <textarea cols="15" rows="2">Présentation :</textarea>
                </a>
                <a href="#" class="student">
                    <!--image,texte-->
                    <img src="./assets/images/User_Avatar_2.png" alt="">
                    <span>Gwladys</span>
                    <div class="social-network">
                        <i class="fab fa-discord"></i>
                        <i class="fab fa-linkedin"></i>
                        <i class="fas fa-at"></i>
                    </div>
                    <textarea cols="15" rows="2">Présentation :</textarea>
                </a>
                <a href="#" class="student">
                    <!--image,texte-->
                    <img src="./assets/images/User_Avatar_2.png" alt="">
                    <span>Josselin</span>
                    <div class="social-network">
                        <i class="fab fa-discord"></i>
                        <i class="fab fa-linkedin"></i>
                        <i class="fas fa-at"></i>
                    </div>
                    <textarea cols="15" rows="2">Présentation :</textarea>
                </a>
                <a href="#" class="student">
                    <!--image,texte-->
                    <img src="./assets/images/User_Avatar_2.png" alt="">
                    <span>Kendal</span>
                    <div class="social-network">
                        <i class="fab fa-discord"></i>
                        <i class="fab fa-linkedin"></i>
                        <i class="fas fa-at"></i>
                    </div>
                    <textarea cols="15" rows="2">Présentation :</textarea>
                </a>
                <a href="#" class="student">
                    <!--image,texte-->
                    <img src="./assets/images/User_Avatar_2.png" alt="">
                    <span>Kilian</span>
                    <div class="social-network">
                        <i class="fab fa-discord"></i>
                        <i class="fab fa-linkedin"></i>
                        <i class="fas fa-at"></i>
                    </div>
                    <textarea cols="15" rows="2">Présentation :</textarea>
                </a>
                <a href="#" class="student">
                    <!--image,texte-->
                    <img src="./assets/images/User_Avatar_2.png" alt="">
                    <span>Killian</span>
                    <div class="social-network">
                        <i class="fab fa-discord"></i>
                        <i class="fab fa-linkedin"></i>
                        <i class="fas fa-at"></i>
                    </div>
                    <textarea cols="15" rows="2">Présentation :</textarea>
                </a>
                <a href="#" class="student">
                    <!--image,texte-->
                    <img src="./assets/images/User_Avatar_2.png" alt="">
                    <span>Lilian</span>
                    <div class="social-network">
                        <i class="fab fa-discord"></i>
                        <i class="fab fa-linkedin"></i>
                        <i class="fas fa-at"></i>
                    </div>
                    <textarea cols="15" rows="2">Présentation :</textarea>
                </a>
                <a href="#" class="student">
                    <!--image,texte-->
                    <img src="./assets/images/User_Avatar_2.png" alt="">
                    <span>Mathis</span>
                    <div class="social-network">
                        <i class="fab fa-discord"></i>
                        <i class="fab fa-linkedin"></i>
                        <i class="fas fa-at"></i>
                    </div>
                    <textarea cols="15" rows="2">Présentation :</textarea>
                </a>
                <a href="#" class="student">
                    <!--image,texte-->
                    <img src="./assets/images/User_Avatar_2.png" alt="">
                    <span>Nathan</span>
                    <div class="social-network">
                        <i class="fab fa-discord"></i>
                        <i class="fab fa-linkedin"></i>
                        <i class="fas fa-at"></i>
                    </div>
                    <textarea cols="15" rows="2">Présentation :</textarea>
                </a>
                <a href="#" class="student">
                    <!--image,texte-->
                    <img src="./assets/images/User_Avatar_2.png" alt="">
                    <span>Romain</span>
                    <div class="social-network">
                        <i class="fab fa-discord"></i>
                        <i class="fab fa-linkedin"></i>
                        <i class="fas fa-at"></i>
                    </div>
                    <textarea cols="15" rows="2">Présentation :</textarea>
                </a>
                <a href="#" class="student">
                    <!--image,texte-->
                    <img src="./assets/images/User_Avatar_2.png" alt="">
                    <span>Thibaud</span>
                    <div class="social-network">
                        <i class="fab fa-discord"></i>
                        <i class="fab fa-linkedin"></i>
                        <i class="fas fa-at"></i>
                    </div>
                    <textarea cols="15" rows="2">Présentation :</textarea>
                </a>
                <a href="#" class="student">
                    <!--image,texte-->
                    <img src="./assets/images/User_Avatar_2.png" alt="">
                    <span>Télio</span>
                    <div class="social-network">
                        <i class="fab fa-discord"></i>
                        <i class="fab fa-linkedin"></i>
                        <i class="fas fa-at"></i>
                    </div>
                    <textarea cols="15" rows="2">Présentation :</textarea>
                </a>
                <a href="#" class="student">
                    <!--image,texte-->
                    <img src="./assets/images/User_Avatar_2.png" alt="">
                    <span>Théo</span>
                    <div class="social-network">
                        <i class="fab fa-discord"></i>
                        <i class="fab fa-linkedin"></i>
                        <i class="fas fa-at"></i>
                    </div>
                    <textarea cols="15" rows="2">Présentation :</textarea>
                </a>
                <a href="#" class="student">
                    <!--image,texte-->
                    <img src="./assets/images/User_Avatar_2.png" alt="">
                    <span>Thomas</span>
                    <div class="social-network">
                        <i class="fab fa-discord"></i>
                        <i class="fab fa-linkedin"></i>
                        <i class="fas fa-at"></i>
                    </div>
                    <textarea cols="15" rows="2">Présentation :</textarea>
                </a>
                <a href="#" class="student">
                    <!--image,texte-->
                    <img src="./assets/images/User_Avatar_2.png" alt="">
                    <span>Tom</span>
                    <div class="social-network">
                        <i class="fab fa-discord"></i>
                        <i class="fab fa-linkedin"></i>
                        <i class="fas fa-at"></i>
                    </div>
                    <textarea cols="15" rows="2">Présentation :</textarea>
                </a>
            </div>
        </div>
    </body>

</html>